from odoo import api, fields, models


class StockProductionLot(models.Model):
    _inherit = "stock.production.lot"

    single_unit_selling_price = fields.Float()
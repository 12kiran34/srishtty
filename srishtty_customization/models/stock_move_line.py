from odoo import api, fields, models


class StockMoveLine(models.Model):
    _inherit = "stock.move.line"

    single_unit_selling_price = fields.Float()


    def _get_value_production_lot(self):
        res = super()._get_value_production_lot()
        if self.expiration_date:
            res.update({
                'single_unit_selling_price': self.single_unit_selling_price or 0,
            })
        return res
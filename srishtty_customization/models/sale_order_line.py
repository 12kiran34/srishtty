from odoo import api, fields, models


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    # lot_id = fields.Many2one("stock.production.lot", "Lot", copy=False)
    tax_percentage = fields.Float(compute='compute_tax_percentage', store=True, compute_sudo=True)

    @api.depends('tax_id', 'tax_id.amount')
    def compute_tax_percentage(self):
        for rec in self:
            tax_percentage = []
            for tax in rec.tax_id:
                tax_percentage.append(tax.amount)
            rec.tax_percentage = tax_percentage and sum(tax_percentage) or 0

    def get_month_year_from_date(self):
        for rec in self:
            date_month_year = ""
            if rec.lot_id.expiration_date:
                date_month = rec.lot_id.expiration_date.month
                date_year = rec.lot_id.expiration_date.year
                date_month_year = str(date_month) + "/" +str(date_year)
            return date_month_year



    # def _prepare_procurement_values(self, group_id=False):
    #     vals = super()._prepare_procurement_values(group_id=group_id)
    #     if self.lot_id:
    #         vals["restrict_lot_id"] = self.lot_id.id
    #     return vals
    #
    # @api.onchange("product_id")
    # def product_id_change(self):
    #     super().product_id_change()
    #     self.lot_id = False
    #
    # @api.onchange("product_id")
    # def _onchange_product_id_set_lot_domain(self):
    #     return {"domain": {"lot_id": [("product_id", "=", self.product_id.id)]}}

    @api.onchange("lot_id")
    def lot_id_change_for_price(self):
        self.price_unit = self.lot_id and self.lot_id.single_unit_selling_price or 0

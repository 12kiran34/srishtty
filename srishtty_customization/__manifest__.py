# -*- coding: utf-8 -*-
{
    'name': "srishtty_customization",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['product_expiry', 'report_xlsx', 'sale_management'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'reports/sale_template.xml',
        'reports/report.xml',
        'wizard/sale_report_view.xml',
        'views/production_lot_views.xml',
        'views/stock_move_views.xml',
        'views/menu.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}

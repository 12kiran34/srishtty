from odoo import models, fields
import pytz

class SaleReportWiz(models.TransientModel):
    _name = 'sale.report.wiz'
    _description = 'Sale Report'

    from_date = fields.Date()
    to_date = fields.Date()
    tax_ids = fields.Many2many('account.tax', 'sale_report_tax_rel', 'sale_wiz_id', 'tax_id')
    report_type = fields.Selection(
        string='Report Type',
        selection=[('retail', 'Retail'),
                   ('wholesale', 'Wholesale'), ],
        required=False, default='retail')


    def convert_date_to_datetime(self, start_date, end_date):
        start_date = fields.Date.from_string(start_date).strftime("%Y-%m-%d 00:00:00")
        end_date = fields.Date.from_string(end_date).strftime("%Y-%m-%d 23:59:59")
        return self.convert_to_utc(str(start_date)), self.convert_to_utc(str(end_date))

    def convert_to_utc(self, input_date):
        start_date = False
        if input_date:
            tz_name = self._context.get('tz', False) or self.env.user.tz or 'Asia/Kolkata'
            date = fields.Datetime.to_string(pytz.timezone(tz_name).localize(fields.Datetime.from_string(input_date), is_dst=None).astimezone(pytz.utc))
        return date

    # @api.multi
    def action_print_report(self):
        for rec in self:
            start_datetime, end_datetime = self.convert_date_to_datetime(rec.from_date, rec.to_date)

            data = {
                'ids': self.ids,
                'model': self._name,
                'form': {
                    'start_date': rec.from_date,
                    'end_date': rec.to_date,
                    'from_date': start_datetime,
                    'to_date': end_datetime,
                    'tax_ids': self.tax_ids and self.tax_ids.ids or []
                },
            }
            if rec.report_type == 'retail':
                return self.env.ref('srishtty_customization.pharmacy_sale_summary_report_xlsx').report_action(self, data=data)
            else:
                return self.env.ref('pharmacy_customization.pharmacy_wholesale_sale_summary_report_xlsx').report_action(self, data=data)
from odoo import fields, models, tools, api
from datetime import date, datetime, timedelta

import pytz
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT as DTF, DEFAULT_SERVER_DATE_FORMAT as DF


class SaleSummaryXlsx(models.AbstractModel):
    _name = 'report.pharmacy_customization.sale_summary_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    @api.model
    def get_datetime_in_tz(self, input_date):
        date_time = str(input_date)
        if date_time:
            time_obj = datetime.strptime(date_time, DTF)
            tz_time = ''
            tz_name = self._context.get('tz', False) or self.env.user.tz or 'Asia/Kolkata'
            if tz_name and time_obj:
                return time_obj.replace(tzinfo=pytz.timezone('UTC')).astimezone(pytz.timezone(tz_name)).strftime(
                    '%d-%m-%Y %H:%M:%S')
            else:
                return False
        else:
            return False


    def generate_xlsx_report(self, workbook, data, lines):
        merge_format = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'fg_color': '#44F2DB'})

        domain = [('state', 'in', ['sale', 'done'])]

        if data['form']['from_date']:
            domain.append(('order_id.date_order', '>=', data['form']['from_date']))
        if data['form']['to_date']:
            domain.append(('order_id.date_order', '<=', data['form']['to_date']))


        sale_line_data = self.env['sale.order.line'].sudo().search(domain)

        report_name = "Sale Summary Report"
        sheet = workbook.add_worksheet(report_name[:30])
        sheet.merge_range('A1:J1', report_name, merge_format)
#         sheet.freeze_panes(3, 0, top_row=1)

        bold = workbook.add_format({'bold': True})
        subheader_common_style = {'bold': True, 'align': 'center', 'font_color': 'white', 'bg_color': '#778899',
                                  'text_wrap': True, 'border': True}
        sub_header = workbook.add_format(subheader_common_style)
        subheader2_style = subheader_common_style
        sub_header_2 = workbook.add_format(subheader2_style)
        common_data = workbook.add_format(
            {'bold': False, 'font_color': 'black', 'text_wrap': True, 'border': True, 'num_format': '#,##0.00',
             'align': 'right'})
        table_subheader = {'bold': False, 'align': 'center', 'font_color': 'black', 'bg_color': '#ADD8E6',
                           'text_wrap': True, 'border': True}
        table_sub_header = workbook.add_format(table_subheader)
        table_subheader2 = {'bold': False, 'align': 'center', 'font_color': 'black', 'bg_color': '#ADD8E6',
                            'text_wrap': True, 'border': True}
        table_sub_header2 = workbook.add_format(table_subheader2)
        report_head = workbook.add_format(
            {'bold': True, 'align': 'center', 'font_color': 'black', 'bg_color': '#ADD8E6', 'text_wrap': True,
             'border': True, 'font_size': 14})
        filter_report_head = workbook.add_format(
            {'bold': False, 'align': 'left', 'font_color': 'black', 'bg_color': '#ADD8E6', 'text_wrap': True,
             'border': True, 'font_size': 12})
        main_head = workbook.add_format(
            {'bold': True, 'align': 'center', 'font_color': 'white', 'bg_color': '#778899', 'text_wrap': True,
             'border': True, 'font_size': 16})
        common_data_2 = workbook.add_format({'bold': False, 'font_color': 'black', 'text_wrap': True, 'border': True})
        bold = workbook.add_format({'bold': True, 'border': True, 'num_format': '#,##0.00'})
        date_format_1 = workbook.add_format({'num_format': 'dd-mm-yy hh:mm:ss', 'border': True})
        sheet.set_row(0, 35)
        sheet.set_row(1, 35)
#         sheet.set_row(2, 25)
        sheet.set_column(0, 0, 4)
        sheet.set_column(1, 1, 12)
        sheet.set_column(2, 2, 12)
        sheet.set_column(3, 3, 12)
        sheet.set_column(4, 4, 16)
        sheet.set_column(5, 5, 13)
        sheet.set_column(6, 6, 10)
        sheet.set_column(7, 8, 10)
        sheet.set_column(9, 15, 10)
        row = 1
        col = 0
        c = 0
        sheet.merge_range('A1:J1', "SRISHTI PHARMA", main_head)
        row = row + 1
        if data['form']['start_date'] and data['form']['end_date']:
            s = datetime.strptime(data['form']['start_date'], '%Y-%m-%d').date()
            e = datetime.strptime(data['form']['end_date'], '%Y-%m-%d').date()
            date_start = s.strftime('%d-%m-%Y')
            date_end = e.strftime('%d-%m-%Y')
            sheet.merge_range('A2:J2', "Sale Summary Report " +date_start + " - To - " + date_end, report_head)

            sheet.set_row(row, 30)
            sheet.write(row, c, "Sl No.", sub_header)
            c = c + 1
            sheet.write(row, c, "Order Reference", sub_header)
            c = c + 1
            sheet.write(row, c, "Customer", sub_header)
            c = c + 1
            sheet.write(row, c, "Date", sub_header)
            c = c + 1
            sheet.write(row, c, "Medicine", sub_header)
            c = c + 1
            sheet.write(row, c, "Bill No.", sub_header)
            c = c + 1
            sheet.write(row, c, "Untaxed Amount", sub_header)
            c = c + 1
            sheet.write(row, c, "Tax Percentage", sub_header)
            c = c + 1
            sheet.write(row, c, "Tax Amount", sub_header)
            c = c + 1
            sheet.write(row, c, "Total", sub_header_2)
            c = c + 1

            # cash_customer = self.env.ref('pharmacy_customization.res_cash_customer')
            sl_no = 1
            row = 3
            for line in sale_line_data:
                sheet.write(row, 0, sl_no, common_data_2)
                sheet.write(row, 1, line.order_id.name or " ", common_data)
                # if line.order_id and line.order_id.partner_id and line.order_id.partner_id == cash_customer:
                #     sheet.write(row, 2, "Cash Customer ", common_data)
                # else:
                sheet.write(row, 2, line.order_id and line.order_id.partner_id and line.order_id.partner_id.name or " ", common_data)
                sheet.write(row, 3, line.order_id.date_order and self.get_datetime_in_tz(line.order_id.date_order) or " ", date_format_1)
                sheet.write(row, 4, line.product_id.name or " ", common_data)
                sheet.write(row, 5, line.order_id.name or " ", common_data)
                sheet.write(row, 6, line.price_subtotal or 0.00, common_data)
                sheet.write(row, 7, line.tax_percentage or 0.00, common_data)
                sheet.write(row, 8, line.price_tax or 0.00, common_data)
                sheet.write(row, 9, line.price_total or 0.00, common_data)
                row += 1
                sl_no += 1
            row += 2
            sheet.write(row, 2, "Tax Percentage", sub_header_2)
            sheet.write(row, 3, "Untaxed Amount", sub_header_2)
            sheet.write(row, 4, "Taxed Amount", sub_header_2)
            sheet.write(row, 5, "Total Amount", sub_header_2)

            groups_data = self.env['sale.order.line'].sudo().read_group(domain, fields=['price_subtotal', 'price_tax', 'price_total'], groupby='tax_percentage')
            for data in groups_data:
                row += 1
                sheet.write(row, 2, data.get('tax_percentage', 0), common_data)
                sheet.write(row, 3, data.get('price_subtotal', 0), common_data)
                sheet.write(row, 4, data.get('price_tax', 0), common_data)
                sheet.write(row, 5, data.get('price_total', 0), common_data)
{
    "name": "Stock Production Lot Date",
    "summary": """
        Define lot date and set expiry date relatively.
    """,
    "author": "Mint System GmbH, Odoo Community Association (OCA)",
    "website": "https://www.mint-system.ch",
    "category": "Inventory",
    "version": "14.0.1.1.0",
    "license": "AGPL-3",
    "depends": ["product_expiry"],
    "data": ["views/stock_production_lot.xml"],
    "installable": True,
    "application": False,
    "auto_install": False,
    "images": ["images/screen.png"],
}

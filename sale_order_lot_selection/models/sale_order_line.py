from odoo import api, fields, models


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    lot_id = fields.Many2one("stock.production.lot", "Lot", copy=False)
    on_hand_qty = fields.Float(digits='Product Unit of Measure')
    lot_qty = fields.Float(string="LOT Available Qty", digits='Product Unit of Measure')

    def _prepare_procurement_values(self, group_id=False):
        vals = super()._prepare_procurement_values(group_id=group_id)
        if self.lot_id:
            vals["restrict_lot_id"] = self.lot_id.id
        return vals

    @api.onchange("product_id")
    def product_id_change(self):
        super().product_id_change()
        self.lot_id = False

    @api.onchange("product_id")
    def _onchange_product_id_set_lot_domain(self):

        lot_list = []
        on_hand_qty = 0
        Quant = self.env['stock.quant'].sudo()._gather(product_id=self.product_id, location_id=self.env.ref('stock.stock_location_stock'))
        for qt in Quant:
            if qt.available_quantity:
                lot_list.append(qt.lot_id.id)
                on_hand_qty += qt.available_quantity

        self.update({
            'on_hand_qty': on_hand_qty
        })

        return {"domain": {"lot_id": [("id", "in", lot_list)]}}

    @api.onchange("lot_id")
    def lot_id_change(self):

        lot_qty = 0
        Quant = self.env['stock.quant'].sudo()._gather(product_id=self.product_id,
                                                       location_id=self.env.ref('stock.stock_location_stock'),
                                                       lot_id=self.lot_id)
        for qt in Quant:
            if qt.available_quantity:
                lot_qty += qt.available_quantity
        # purchase_price = 0
        # for lt in self.lot_id:
        #     for po in lt.purchase_order_ids

        move_id = self.env['stock.move.line'].sudo().search([
            ('lot_id', '=', self.lot_id.id),
            ('move_id.location_id', '=', self.env.ref('stock.stock_location_suppliers').id),
            ('move_id.purchase_line_id', '!=', False)
        ], limit=1)

        purchase_price = move_id.move_id and move_id.move_id.purchase_line_id.price_subtotal/ (move_id.move_id.purchase_line_id.product_qty * move_id.move_id.purchase_line_id.product_uom.factor_inv) or 0
        # print('======', purchase_price, move_id.move_id.purchase_line_id.price_total)
        self.update({
            'lot_qty': lot_qty,
            'purchase_price': purchase_price
        })

    @api.onchange("product_uom_qty")
    def product_uom_qty_change_srishtty(self):
        for rec in self:
            rec.lot_id_change_for_price()



